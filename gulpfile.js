var gulp = require('gulp');
var babel = require('gulp-babel');
var connect = require("gulp-connect");
/* 下面的是为了让浏览器支持import和export，babelify这个放到了package.json中了 */
var browserify= require('browserify');
var source = require('vinyl-source-stream');

gulp.task('browserify', function() {
return browserify('src/index.js').bundle().pipe(source('index.js')).pipe(gulp.dest('dist'));
});

// gulp.task('build', () => {
//    gulp.src('src/./*.js')
//       .pipe(babel())
//       .pipe(gulp.dest('./dev'))
// });

// gulp.task('watch', () => {
//    gulp.watch('./*.js', ['build']);
// });

// gulp.task("connect", function () {
//    connect.server({
//       root: ".",
//       livereload: true
//    });
// });

// gulp.task('start', ['build', 'watch', 'connect']);